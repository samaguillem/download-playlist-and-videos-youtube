import os
from googleapiclient.discovery import build
from pytube import YouTube

# Enter your YouTube API Key here
api_key = input("Enter your YouTube API Key: ")

def get_youtube_playlist_videos(api_key, playlist_id):
    # Build a YouTube API service using the provided API Key
    youtube = build('youtube', 'v3', developerKey=api_key)

    videos = []

    # Make successive calls to get all paginated items from the playlist
    nextPageToken = None
    while True:
        playlist_items = youtube.playlistItems().list(
            part='snippet',
            playlistId=playlist_id,
            maxResults=50,  # Get up to 50 results in each call (maximum allowed)
            pageToken=nextPageToken
        ).execute()

        # Process results of the current call
        videos.extend(extract_video_urls(playlist_items))

        # Update the page token for the next iteration or exit the loop if no more pages
        nextPageToken = playlist_items.get('nextPageToken')
        if not nextPageToken:
            break

    return videos

def extract_video_urls(playlist_items):
    # Extract video URLs from playlist items
    return [f'https://www.youtube.com/watch?v={item["snippet"]["resourceId"]["videoId"]}' for item in playlist_items.get('items', [])]

def download_and_save_video(video_url, output_path):
    try:
        # Create a YouTube object and get the first mp4 stream
        yt = YouTube(video_url)
        video_stream = yt.streams.filter(file_extension='mp4').first()

        print(f"Downloading: {yt.title}")

        # Download the video and save it to the specified output path
        video_stream.download(output_path, filename=yt.title)

    except Exception as e:
        print(f"Error downloading {video_url}: {e}")
        # Save the link and chapter name to a file in case of an error
        error_filename = f"error_{yt.title}.txt"
        with open(os.path.join(output_path, error_filename), 'w') as error_file:
            error_file.write(f"Link not downloaded: {video_url}\n")
            error_file.write(f"Error: {str(e)}\n")

def main():
    # Ask the user for the YouTube playlist or video URL and the output directory name
    playlist_url = input("Enter the YouTube playlist or video URL: ")
    output_folder = input("Enter the name of the directory to save the videos (or a full path): ")

    # Output directory for downloaded videos
    if os.path.isabs(output_folder):
        output_folder_path = output_folder
    else:
        # If only a name is provided, create the directory in the user's home folder
        user_home = os.path.expanduser("~")
        output_folder_path = os.path.join(user_home, output_folder)

    # Create the folder if it does not exist
    os.makedirs(output_folder_path, exist_ok=True)

    if "list=" in playlist_url:
        # Get the playlist ID from the URL
        playlist_id = playlist_url.split("list=")[1].split("&")[0]

        videos = get_youtube_playlist_videos(api_key, playlist_id)

        # Download and save each video
        for video_url in videos:
            download_and_save_video(video_url, output_folder_path)
    else:
        # If the URL is not a playlist, download the video directly
        download_and_save_video(playlist_url, output_folder_path)

if __name__ == "__main__":
    main()
